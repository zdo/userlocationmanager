Pod::Spec.new do |s|
  s.name         = "UserLocationManager"
  s.version      = "1.10"
  s.summary      = "Manager for user location (GPS-based or the any custom one)"
  s.platform = :ios
  s.ios.deployment_target = '11.0'
  s.requires_arc = true

  s.description  = <<-DESC
Manager for user location (GPS-based or the any custom one).

You may specify eather using the CLLocationManager or your custom location (like a city or something like that).
                   DESC

  s.homepage     = "https://gitlab.com/zdo/userlocationmanager"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "Dmitry Zganyaiko" => "zdo.str@gmail.com" }
  s.source       = { :git => "https://gitlab.com/zdo/userlocationmanager.git", :tag => "#{s.version}" }

  s.source_files  = "UserLocationManager/**/*.swift"
  s.swift_version = "4.2"

  s.framework = "UIKit"

end
