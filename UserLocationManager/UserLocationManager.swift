//
//  UserLocationManager.swift
//  UserLocationManager
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

public enum UserLocationManagerError: Error {
    case noGpsPermission

    public var localizedDescription: String {
        switch self {
        case .noGpsPermission:
            return "No GPS permission"
        }
    }
}

public class UserLocationManager : NSObject, CLLocationManagerDelegate {
    static public let errorUserInfoKey = "error"
    static public let updateNotification = NSNotification.Name("UserLocationManager.update")
    static public let errorNotification = NSNotification.Name("UserLocationManager.error")
    static public let gpsPermissionChange = NSNotification.Name("UserLocationManager.gpsPermissionChange")

    public static let shared = UserLocationManager()

    private var isLoading = false

    public var selectedCustomLocation: CustomUserLocation? {
        didSet {
            guard !self.isLoading else { return }

            self.save()
            NotificationCenter.default.post(name: UserLocationManager.updateNotification, object: self)
        }
    }

    private var gps: CLLocationManager?

    public var userLocation: CLLocation? {
        if let l = self.selectedCustomLocation {
            return l.location
        } else {
            return self.gps?.location
        }
    }

    override public init() {
        super.init()

        self.load()
        self.updateGpsManager()

        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: nil) { [weak self] (n) in
            self?.updateGpsManager()
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    private func updateGpsManager() {
        if self.selectedCustomLocation == nil {
            if self.gps == nil {
                let newGps = CLLocationManager()
                self.gps = newGps

                newGps.delegate = self
            }

            let authStatus = CLLocationManager.authorizationStatus()
            switch authStatus {
            case .denied, .restricted:
                NotificationCenter.default.post(name: UserLocationManager.errorNotification, object: self,
                                                userInfo: [UserLocationManager.errorUserInfoKey: UserLocationManagerError.noGpsPermission])
            case .notDetermined:
                self.gps?.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                // All right.
                self.gps?.startUpdatingLocation()
                break
            }

            self.gps?.requestLocation()
        } else {
            self.gps?.stopUpdatingLocation()
            self.gps?.stopUpdatingHeading()
            self.gps?.delegate = nil
            self.gps = nil
        }
    }

    private func save() {
        if let l = self.selectedCustomLocation {
            UserDefaults.standard.set(l.toDict(), forKey: "LocationManager.customLocation")
        } else {
            UserDefaults.standard.removeObject(forKey: "LocationManager.customLocation")
        }
    }

    private func locationFromUserDefaultsWith(key: String) -> CustomUserLocation? {
        var result: CustomUserLocation?

        if let customLocationDict = UserDefaults.standard.dictionary(forKey: key) {
            result = CustomUserLocation.createFrom(dict: customLocationDict)
        }

        return result
    }

    private func load() {
        self.isLoading = true
        self.selectedCustomLocation = self.locationFromUserDefaultsWith(key: "LocationManager.customLocation")
        self.isLoading = false
    }

    // MARK: - CLLocationManagerDelegate

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LocationManager error: \(error)")
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        NotificationCenter.default.post(name: UserLocationManager.updateNotification, object: self)
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            NotificationCenter.default.post(name: UserLocationManager.gpsPermissionChange, object: self)
        }
    }
}

public struct CustomUserLocation : Equatable {
    public private(set) var id: String
    public private(set) var title: String
    public private(set) var location: CLLocation

    public init(id: String, title: String, location: CLLocation) {
        self.id = id
        self.title = title
        self.location = location
    }

    public static func == (lhs: CustomUserLocation, rhs: CustomUserLocation) -> Bool {
        return lhs.id == rhs.id
    }

    internal func toDict() -> [String: Any] {
        return [
            "id": self.id,
            "title": self.title,
            "location": [
                "latitude": self.location.coordinate.latitude,
                "longitude": self.location.coordinate.longitude
            ]
        ]
    }

    static internal func createFrom(dict: [String: Any]) -> CustomUserLocation {
        let locationDict = dict["location"] as! [String: Any]
        return CustomUserLocation(id: dict["id"] as! String,
                                  title: dict["title"] as! String,
                                  location: CLLocation(latitude: locationDict["latitude"] as! CLLocationDegrees,
                                                       longitude: locationDict["longitude"] as! CLLocationDegrees))
    }
}
